const {
  markSingleClaimOnCanvas,
  cleanInput,
  markClaims,
  findDuplicates
} = require("./d3");

describe("cleanInput", () => {
  it("should clean up the inputs", () => {
    const input = "#1 @ 0,0: 1x1.#2 @ 1,2: 3x4";

    const outcome = [
      { id: "#1", posX: 0, posY: 0, width: 1, height: 1 },
      { id: "#2", posX: 1, posY: 2, width: 3, height: 4 }
    ];

    const result = input.split(".").map(cleanInput);

    expect(result).toEqual(outcome);
  });
});

describe("markSingleClaimOnCanvas", () => {
  it("one claim should be marked on canvas", () => {
    const input = {
      canvas: [],
      params: { id: "#1", posX: 0, posY: 0, width: 1, height: 1 }
    };

    const outcome = [["#1"]];

    const result = markSingleClaimOnCanvas(input);

    expect(result).toEqual(outcome);
  });
});

describe("markClaims", () => {
  it("given claims should be marked", () => {
    const input = [
      { id: "#1", posX: 0, posY: 0, width: 1, height: 1 },
      { id: "#2", posX: 0, posY: 0, width: 2, height: 2 },
      { id: "#3", posX: 1, posY: 1, width: 2, height: 1 }
    ];

    const outcome = [["#1#2", "#2"], ["#2", "#2#3", "#3"]];

    const result = markClaims(input);

    expect(result).toEqual(outcome);
  });
});

describe("findDuplicates", () => {
  it("should count the number of duplicate taken sports", () => {
    const input = [["#1#2", "#2"], ["#2", "#2#3", "#3"]];

    const outcome = 2;

    const result = findDuplicates(input);

    expect(result).toEqual(outcome);
  });
});

describe("solve day 2 part 1", () => {
  it("given a list of claim", () => {
    const input = "#1 @ 1,3: 4x4.#2 @ 3,1: 4x4.#3 @ 5,5: 2x2";
    const outcome = 4;

    const cleansedInput = input.split(".").map(cleanInput);
    const canvasWithMarkedClaims = markClaims(cleansedInput);
    const duplicates = findDuplicates(canvasWithMarkedClaims);
    console.log(duplicates);

    const result = 4;

    expect(result).toEqual(outcome);
  });
});
