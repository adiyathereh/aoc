const _ = require("lodash");

const markSingleClaimOnCanvas = ({ canvas, params }) => {
  const { id, posX, posY, width, height } = params;

  for (let x = posX; x < height + posX; x++) {
    for (let y = posY; y < width + posY; y++) {
      if (canvas[x] === undefined) {
        canvas[x] = [];
      }

      // if there is already something in the cell add id to it
      canvas[x][y] = canvas[x][y] === undefined ? id : canvas[x][y] + id;
    }
  }

  return canvas;
};

const cleanInput = input => {
  const map = {
    "#": "id",
    ",": {
      0: "posX",
      1: "posY"
    },
    x: {
      0: "width",
      1: "height"
    }
  };

  return input.split(" ").reduce(
    (acc, el) => {
      if (el.includes("@")) {
        return acc;
      }

      let mapper = "";
      if (el.includes("#")) {
        // id
        acc.id = el;
      } else {
        if (el.includes(",")) {
          // position
          mapper = map[","];
          el = el.replace(":", "").split(",");
        } else if (el.includes("x")) {
          // size
          mapper = map["x"];
          el = el.split("x");
        }

        acc[mapper[0]] = Number(el[0]);
        acc[mapper[1]] = Number(el[1]);
      }
      return acc;
    },
    { id: "", posX: 0, posY: 0, width: 0, height: 0 }
  );
};

const markClaims = input => {
  return input.reduce(
    (canvas, params) => markSingleClaimOnCanvas({ canvas, params }),
    []
  );
};

const findDuplicates = input => {
  return _.flattenDeep(input).filter(el => (el ? el.length > 2 : "")).length;
};

module.exports = {
  markSingleClaimOnCanvas,
  cleanInput,
  markClaims,
  findDuplicates
};
