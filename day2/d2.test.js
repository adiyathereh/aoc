const {
  sortIds,
  isLetterRepeatedNTimes,
  getChecksum,
  filterRepetitions,
  getRepeatedLetters,
  hasOneDifference,
  findCommonLetters,
  compareAllPairs
} = require("./d2");

// PART 1

// describe("isLetterRepeatedNTimes", () => {
//   it("should find repeated letters in string", () => {
//     const tripletteInput = {
//       string: "ababab",
//       letter: "a"
//     };

//     const dupletteInput = {
//       string: "abcdee",
//       letter: "e"
//     };

//     const resultWithTriplette = isLetterRepeatedNTimes(
//       tripletteInput.string,
//       tripletteInput.letter
//     )(3);

//     const resultWithDuplette = isLetterRepeatedNTimes(
//       dupletteInput.string,
//       dupletteInput.letter
//     )(2);

//     expect(resultWithTriplette).toBe(true);
//     expect(resultWithDuplette).toBe(true);
//   });
// });

// describe("getRepeatedLetters", () => {
//   it("return the number of repeated letters", () => {
//     const input = ["aabbcccd", "a"];

//     const outcome = { duplicates: true, triplettes: true };

//     const result = getRepeatedLetters(...input);

//     expect(result).toEqual(outcome);
//   });
// });

// describe("filterRepetitions", () => {
//   it("filter out repeated letters", () => {
//     const input = "aabbcccd";

//     const outcome = ["a", "b", "c", "d"];

//     const result = filterRepetitions(input);

//     expect(result).toEqual(outcome);
//   });
// });

// describe("getChecksum", () => {
//   it("should find repeated letters in string", () => {
//     const input = "abcdef, bababc, abbcde, abcccd, aabcdd, abcdee, ababab";

//     const outcome = 12;

//     const result = getChecksum(input);

//     // .toEqual for deep equality
//     expect(result).toBe(outcome);
//   });
// });

// PART 2

describe("hasOneDifference", () => {
  it("should indicate if ids have 1 different letter in the same position", () => {
    const inputWithOneDiff = ["fghij", "fguij"];
    const inputWithTwoDiffs = ["faaij", "fguij"];

    const resultWithOneDiff = hasOneDifference(inputWithOneDiff);
    const resultWithTwoDiffs = hasOneDifference(inputWithTwoDiffs);

    // .toEqual for deep equality
    expect(resultWithOneDiff).toBe(true);
    expect(resultWithTwoDiffs).toBe(false);
  });
});

describe("findCommonLetters", () => {
  it("should find common letters", () => {
    const input = ["fghij", "fguij"];

    const outcome = "fgij";

    const result = findCommonLetters(input);

    expect(result).toBe(outcome);
  });
});

describe("compareAllPairs", () => {
  it("should go through all ids and find the pair with the common letters", () => {
    const input = "abcde,fghij,klmno,pqrst,fguij,axcye,wvxyz";

    const outcome = ["fghij", "fguij"];

    const result = compareAllPairs(input);

    expect(result).toEqual(outcome);
  });
});

describe("solve part 2", () => {
  it("should go through all ids and give back correct answer", () => {
    const input =
      "qysdgimlcaghpfozuwejmhrbvx,qysdtiklcagnpfhzuwbjmhrtvx,qysdtiflcsgnpfozuwejmhruvx,qkshtiklnagnpfozuwejmhrbvx,qysdtnklcagnpmozuwejmhrrvx,qysdttkecagnpfozuwijmhrbvx,qyedtiklcagnvfozuweymhrbvx,qyzdtikzcagnpfozuwejmhqbvx,qysdtiklcagnpfozlwedmhqbvx,qjsdtiklcagnpfozubejmhrbvq,qysdtiklcagnpfozvvejmhrbex,qdsdziklcagnpfouuwejmhrbvx,qysttikqccgnpfozuwejmhrbvx,qysdtiklcagnpbozwwecmhrbvx,qysdtiklcagnpfozuwexmmrjvx,nysdtiklcqgjpfozuwejmhrbvx,cysdoiqlcagnpfozuwejmhrbvx,qysdthxlcagnpfozuwejmcrbvx,qyswtiklcrgnpfozuwejmhrbvf,qysdtiklcagnpfozurejmhvpvx,qysdtiklcegnpfdzuwejghrbvx,qysdtjkluagnpfozuwenmhrbvx,qysdtimlcagnpjwzuwejmhrbvx,qyrdtiklcegnpeozuwejmhrbvx,qysdmiklcagnpfokswejmhrbvx,qysdtizlcagnpiozuwecmhrbvx,qysdtiklcignafxzuwejmhrbvx,qycdjiklcagnpzozuwejmhrbvx,qysdtiklcagnpjozuwepihrbvx,qyedtiklcrgnpfozuvejmhrbvx,mysdtikrcagnpfozwwejmhrbvx,qysdtiklcagnpfozuhcjmhrbsx,qmsdtiklcagnpfozuwehmhrevx,qgsdtiklcagnpfozuwejvhrbvp,lysdtikleagnpfozuwejmhrnvx,qxsdtivlzagnpfozuwejmhrbvx,qysdtiklcoggpfozuwebmhrbvx,wysdtiklcagnpfozuwejmornvx,jysdtiklvagntfozuwejmhrbvx,qmsdtiklcagnpfozuwejmrrbax,qysdttklcagnpfoiuwejmhrbvh,qysdtnklcaenpfozupejmhrbvx,qysdtoklcagnpfozuwexmhrbvq,qysdtiklcagnpuoeuwejmhrjvx,iysdtitncagnpfozuwejmhrbvx,qysdtixrcagnprozuwejmhrbvx,qyfdtiplcagnpfouuwejmhrbvx,qysdtmklcagnpfowuwejmhrbox,qysdtiklcagnxiozuwejphrbvx,fysdtiklcagnptozuwejmhrbvo,qysdqiklcagnplozuwejmhmbvx,qysdtwkacagnpfosuwejmhrbvx,qysdtitlcagnpfozufajmhrbvx,qysdtcklcagopfdzuwejmhrbvx,qmfdtdklcagnpfozuwejmhrbvx,qysztiklcaonpfozuwejmhrbfx,qygdtiklcggnpfozuwejmhrhvx,qysdiiklcagnpfozukejmcrbvx,qysdtrkloagnpfozuwujmhrbvx,qycdtiklcagnpfozywejmhrlvx,qgsdtikdcagnpfozgwejmhrbvx,qyudtikleagvpfozuwejmhrbvx,pysdtiqlcagnpfozuwejmarbvx,qysdtiklcaenpfozuwehahrbvx,qhsttiklcagnpfovuwejmhrbvx,zysdtikqmagnpfozuwejmhrbvx,rysdtikacagnpfozuwevmhrbvx,zysntikllagnpfozuwejmhrbvx,qysttimlcagndfozuwejmhrbvx,qysdtiklcaxopfqzuwejmhrbvx,qysdtislcagnpfozuwejmtrbnx,qysdviklcagnpfozswejmhibvx,qmsdtiklrygnpfozuwejmhrbvx,qysztiklcagnpfozuwejmorbrx,xysdtiklcagnzwozuwejmhrbvx,qysjthklcagnpfozowejmhrbvx,qysdtiklcagnpfofxwemmhrbvx,jysdtiklcagnpfozfwehmhrbvx,qysdtgklaagnpfozhwejmhrbvx,qqsdtiklcaenpfozuwejmhrvvx,qysdtikloajppfozuwejmhrbvx,qysdtiklcagnpwozuwejmhrhsx,qpsdtiklcapnprozuwejmhrbvx,qyzdtiklcagnpcozuwejmhrbvc,qusdhiklcagnpfozuwejmhrbxx,qysdtiklcagnpfozqwejthrvvx,qysvtiklcagnpfoiuwedmhrbvx,qgsdtiklcagvpfozuwejmhrbvf,qysdtikxcawnpfozuwejmarbvx,qyvctiklcaynpfozuwejmhrbvx,qyyltiklnagnpfozuwejmhrbvx,oysdtillcagnpfozuwejmnrbvx,qysdtiklcagnpfozuvmjmhrbzx,qykdtikocagnpfhzuwejmhrbvx,qysdtvkloagnpfozuwejmkrbvx,qysetiklcagnpfozuwejmhrxvh,qysdtiklcavnpfuzuwejmhrbvh,qmndtiklcagnpfojuwejmhrbvx,qysdtialcagnpfozuwejmdrqvx,qysdtiklcagnpfozuwejtzrbvv,qysdtiklxagnpyozufejmhrbvx,qysdtiklcagnpfgzewejahrbvx,qysdtiklcagppsozuwejmhrdvx,qykdtiklcainpfozuwejqhrbvx,qysdtiklcagnpfszxwejmhubvx,qyrdtiklcagkptozuwejmhrbvx,qysdsiklcagnpfozsvejmhrbvx,qypdtiklcagypfozuwejmhrlvx,qssdtiklcagnpfozuwqjmirbvx,qyshtiklcagnpfrzuwyjmhrbvx,qysdtiklcagnpfqzuwenmgrbvx,qysdtiklcagnpfonuwejmhkwvx,qysdhiklcagnpfokuwejmhrfvx,jysrtiklcaenpfozuwejmhrbvx,qysdtiilcagnpfozuwejmhcbvl,qysdtiklcagnheozuwejmhrbvn,qysdtikucagwpfojuwejmhrbvx,qysdtinlctgnpfozuwujmhrbvx,qysdtiklcagnpiozuwejmtrbjx,qysktiklcagqpfozuwcjmhrbvx,qysddiklcagnpfozpwejmhrbvh,wysdtiplcagnpfozuwejyhrbvx,qysdtiklcagnpfjzlwejmhrcvx,qysdtikleagopbozuwejmhrbvx,qysdtqklcwgnpfozuwejmirbvx,qysdtiklcugnpmozuwejmhrbvp,qysdtiklcagnpfozpwejmnrbvz,qysdtiklcagnpcozuwejmhbbmx,uysitiklcagnpfozewejmhrbvx,qykdtiklcasnpfozuwejdhrbvx,qyjdtiklcagnpqozuzejmhrbvx,qysdtiklcagaifozuwejmhrbvh,qysdtiklcagnhfozuwyjrhrbvx,qysetiklcaanpfozuwyjmhrbvx,qyfdtiklcagnphozulejmhrbvx,qysdtikkcrgnpfozuwejmhpbvx,qysdtiklcarnpfdzuwejmhrbvq,qysdtiklcfyrpfozuwejmhrbvx,rysdtitlcagnpfoznwejmhrbvx,qysdtiilcagnffozugejmhrbvx,qysdyifloagnpfozuwejmhrbvx,qysdtiklcegnpfozuwejmlrcvx,qysdtiklcagnpfozuwajmhbbqx,qysptrklcarnpfozuwejmhrbvx,qysdtiklcagnldozuwejmhwbvx,qysdtiklczgqpfozuwejmhobvx,qyxdtiklcagcpfoiuwejmhrbvx,qysatiklczgnpfozawejmhrbvx,qysduiklcagnpfoziwejyhrbvx,qysdtgklqagnpfozujejmhrbvx,qysdtiqlcagnpfozzdejmhrbvx,qysdtiklcngnpfofuwejmzrbvx,qysdtiklcagnyfozuwejrnrbvx,qysdtiplcagnpfozowmjmhrbvx,qyswtiklcagnplozuwedmhrbvx,qyseiiklcagnpfozuwejmhibvx,qysdtiklcagnpfozutusmhrbvx,qysdtimlcagnpfozccejmhrbvx,qnsdniklcagnpfobuwejmhrbvx,qysrtiklcagnpfofuwejmhrbyx,qyzdtiklcagnpfoizwejmhrbvx,qysdtjslcdgnpfozuwejmhrbvx,qysdtiklcagnpxoyuwejmrrbvx,qysdtikllagnpfmzuwbjmhrbvx,qysdtitlcagnkfozuwejwhrbvx,qymdtiklcggnpfozuwejmzrbvx,qysdtiklclfnpfozuhejmhrbvx,qysdtyklcagnpfozuwejmhhbix,qysetiklcagnpfozuwejmhrspx,qysdipklcagnpfozuwejmhrbex,uysgtiklcagnpmozuwejmhrbvx,qysdtiklmagnpfozuwqlmhrbvx,qysdtiklcagnyfozxwejmhrmvx,qysutillcagnpfozuwejmhrbbx,casdtiklcagnpfopuwejmhrbvx,qesdtiklctgnpfmzuwejmhrbvx,qysdtiklcagopfozjwejmdrbvx,jzsdtiklcagnpfozuwejmurbvx,qysdtiklcjgnpfonuwejrhrbvx,qysdtiklcrgnpnozuwejmhqbvx,oyhdtiklcagnpfozuwekmhrbvx,qysstiklcagjpfozuwejmhrbnx,qyudtiklsagnpsozuwejmhrbvx,qysdtiilcagnpfozusejmhrbva,qysdtiklcaknpfozmwejmhgbvx,qysdbiklcpgnpfozuwejmrrbvx,qybdtiklcagvpfokuwejmhrbvx,qysatiklcagnpwofuwejmhrbvx,qysdtiklcadnpfonuwejmcrbvx,qysdtijfcagnpfozuvejmhrbvx,qysdtiklcagnpfhluuejmhrbvx,qysdtiklcagnpfoguwejqhrwvx,qlsdtiklcagnpfojuwehmhrbvx,qyhdtiolcagnpfozuwejmhrzvx,qmsdtiklcagnppozuwpjmhrbvx,qysdtiklvvgnpfvzuwejmhrbvx,qysdtiklcagnpfszuwajmhrcvx,qysdtiklcagnpfmzuwekmhrbyx,qysdtiklcagwpfozumevmhrbvx,qysdtaklcagnpfozuwejvhzbvx,qysotiklcagntffzuwejmhrbvx,qysdtiklcagnpfowuweqmhrivx,qysdtrkloagnxfozuwujmhrbvx,qasdiiklcagnpfozuwegmhrbvx,qysbtiklcagnpfozuwejthrbhx,hysdtikllagnpfozuwejmhrbbx,qyqdtiklcagnpsozuwejmcrbvx,qysdtiklcagnpiqzuwejmhrbux,qnsdtiklcagnpfozdwejmhbbvx,qysjbiklcagzpfozuwejmhrbvx,qysdtiklcagnpfifuwejmhrbvg,qysdtiklcaggpkozunejmhrbvx,qxsdtiklcavnpfozuwfjmhrbvx,qysdtikycabnpfkzuwejmhrbvx,qyswtzklcagnpfozuwejmhrlvx,qysdtikqcagnpfozuwejrhnbvx,qysdtiplaagnpfozuwejmhruvx,qjcdtiklcagnpfozujejmhrbvx,nysdtyklcagnpfozutejmhrbvx,qysrtiklcagnpfnzuwejmhrbdx,zysdtielcagnpfozuwezmhrbvx,qysdtikpvagnpfozuzejmhrbvx,qysdwiklcagnpfozueejmhrlvx,dysdmiklcagnpfozuwejzhrbvx,qysdtiklcjgnpfozuweimhmbvx,qysdtiklciynpyozuwejmhrbvx,qksdtiklcagnpbozubejmhrbvx,qysdtiklkagnpfozuwejmhrjvb,yyxdtiklcagnpfomuwejmhrbvx,qysdtiklcagnfnozuwejmhrbvv,qysdtzklcagnpfozuwejmhrmvb,qysduiklclgnpfozuwejmhrbvn,qyndtmklcavnpfozuwejmhrbvx,qisdkiklcagnpfozuwqjmhrbvx,qysdtrkycagypfozuwejmhrbvx,qhsdtiklcwgnmfozuwejmhrbvx,qysdaiklcannpfozupejmhrbvx,zysdtiklcagnpjozuwejmhrbwx,qysdtikxcagnpfozuwejmcrxvx,qysdtzklcagnpfozewejmhrbvk,qysdwtklcagnhfozuwejmhrbvx,qysdtqklcaenpfozuwejmdrbvx,qysdtiklcagnpfozuoeemhqbvx,nysdtikocagnpfozuwejmhwbvx,qysxtiklcagnpfozqwejmhrbax,qysdtielcasnpfozuwejmhsbvx,qysdtiklcaknpfozuwejcwrbvx,qysytiklcagnpfozdfejmhrbvx,qysdtiklcagmpfozuwejmgrbox,qysdtielcagnpfpzuwejhhrbvx";

    const outcome = "qysdtrkloagnfozuwujmhrbvx";

    const result = (input => {
      return findCommonLetters(compareAllPairs(input));
    })(input);

    expect(result).toEqual(outcome);
  });
});
