// const isLetterRepeatedNTimes = (string, letter) => {
//   const regex = new RegExp(`[^${letter}]`, "g");

//   const letterRepetitions = string.replace(regex, "");

//   return function(repetitions) {
//     return letterRepetitions.length === repetitions;
//   };
// };

// const removeLetter = (word, letter) => {
//   const regex = new RegExp(`${letter}`, "g");

//   const strippedWord = word.replace(regex, "");

//   return strippedWord;
// };

// const getRepeatedLetters = string => {
//   const uniqueLetters = filterRepetitions(string);
//   const repeatedLetters = uniqueLetters.reduce(
//     (acc, val) => {
//       if (isLetterRepeatedNTimes(string, val)(2)) {
//         acc.duplicates = true;
//       } else if (isLetterRepeatedNTimes(string, val)(3)) {
//         acc.triplettes = true;
//       }
//       return acc;
//     },
//     {
//       duplicates: false,
//       triplettes: false
//     }
//   );

//   return repeatedLetters;
// };

// const filterRepetitions = input => {
//   return Array.from(new Set(input.split("")));
// };

// const getChecksum = input => {
//   let duplicatesCount = 0;
//   let triplettesCount = 0;

//   input.split(",").forEach((id, ind) => {
//     const repeatedLetters = getRepeatedLetters(id);
//     if (repeatedLetters.duplicates === true) {
//       duplicatesCount += 1;
//     }
//     if (repeatedLetters.triplettes === true) {
//       triplettesCount += 1;
//     }
//   });

//   return duplicatesCount * triplettesCount;
// };

const hasOneDifference = ([word, comparator]) => {
  let differences = 0;

  for (let i = 0; i < word.length; i++) {
    if (comparator[i] !== word[i]) {
      differences++;

      if (differences > 1) {
        return false;
      }
    }
  }

  return true;
};

const findCommonLetters = ([wordOne, wordTwo]) => {
  return wordOne
    .split("")
    .filter((letter, ind) => letter === wordTwo[ind])
    .join("");
};

const compareAllPairs = input => {
  const idsToCheck = input.slice().split(",");
  return idsToCheck.filter((id, ind) => {
    for (let i = 0; i < idsToCheck.length; i++) {
      if (ind !== i && hasOneDifference([id, idsToCheck[i]])) {
        return [id, idsToCheck[i]];
      }
    }
  });
};

module.exports = {
  // isLetterRepeatedNTimes,
  // removeLetter,
  // getRepeatedLetters,
  // filterRepetitions,
  // getChecksum,
  hasOneDifference,
  findCommonLetters,
  compareAllPairs
};
