function parseNumber(numberAsString) {
  const number = Number(numberAsString.slice(1));
  if (numberAsString[0] === "+") {
    return number;
  } else {
    return -number;
  }
}

const getFrequency = deltas => {
  return deltas
    .split(" ")
    .map(parseNumber)
    .reduce((acc, number) => {
      return acc + number;
    }, 0);
};

// reduce == foldLeft
const foldLeft = (fn, initialState, collection) => {
  let state = initialState;
  for (let element of collection) {
    state = fn(state, element);
  }
  return state;
};

// scanLeft
const scanLeft = (fn, initialState, collection) => {
  let allStates = [initialState];
  for (let element of collection) {
    allStates.push(fn(allStates[allStates.length - 1], element));
  }
  return allStates;
};

const findFirstRepeatedFrequency = deltasAsStrings => {
  // deltas are the [1,  -2,  3,  1]
  let deltas = deltasAsStrings.split(" ").map(parseNumber);

  let history = new Set();
  let currentFrequency = 0;
  // [ 1, -1, 2, 3, 4, 2, 5, 6, 7, 5, 8, 9]

  let index = 0;
  while (true) {
    const currentDelta = deltas[index];

    currentFrequency += currentDelta;

    if (history.has(currentFrequency)) {
      return currentFrequency;
    }

    history.add(currentFrequency);

    index = (index + 1) % deltas.length;
  }
};

module.exports = {
  getFrequency,
  findFirstRepeatedFrequency
};
